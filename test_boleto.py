import os

import img2pdf as converte_para_pdf
from pdf2image import pdf2image

dir_filename = r'C:\temp'


for pdf_file in os.listdir(dir_filename):
    if pdf_file.endswith(".pdf"):
        pages = pdf2image.convert_from_path(pdf_file, 500)
        for page in pages:
            page.save('test_jpg.jpg','JPEG')
